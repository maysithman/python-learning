from django.contrib import admin
from .models import Hotel, Offer


class HotelAdmin(admin.ModelAdmin):
    list_display = ('name', 'desc', 'price')


class OfferAdmin(admin.ModelAdmin):
    list_display = ('code', 'desc', 'discount')


admin.site.register(Hotel, HotelAdmin)
admin.site.register(Offer, OfferAdmin)

