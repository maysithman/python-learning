from django.db import models


class Hotel(models.Model): 
    name = models.CharField(max_length=100)
    img = models.ImageField(upload_to='pics')
    desc = models.TextField()
    price = models.FloatField()


class Offer(models.Model):
    code = models.CharField(max_length=100)
    desc = models.TextField()
    discount = models.FloatField()

