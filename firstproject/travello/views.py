from django.shortcuts import render
from .models import Destination
from django.contrib.auth.models import User
from hotels.models import Hotel


def index(request):
    destinations = Destination.objects.all()
    users = User.objects.all()
    hotels = Hotel.objects.all()

    return render(request, 'index.html', 
        {
            'destinations': destinations,
            'hotels': hotels,
            'users': users
            })

