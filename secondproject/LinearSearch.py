#find number at the existing list by "Linear Search".


pos = -1


def search(data, find_num):
    i = 0
    while i < len(data):
        if data[i] == find_num:
            globals()['pos'] = i
            return True
        i = i+1
    return False


#give the equal number of apples to the student
def giveAppleToStudent():
    num_of_apples = int(input('Insert The Number of Apples that you have : '))
    num_of_students = int(input('Insert the number of students in the class : '))

    apple_per_student = int(num_of_apples / num_of_students)
    print('Each Student gets ' + str(apple_per_student))


try:
    data = [2, 4, 1, 9, 7]
    find_num = int(input('Insert Number that you want to find at the list : '))

    if search(data, find_num):
        print('Found at the ' + str(pos) + ' index of list.')
    else:
        print('Not Found.')

    giveAppleToStudent() 
except ValueError as e:
    print('Invalid Input Type. ', e)
except ZeroDivisionError as e:
    print('Cannot division by zero.', e)
except Exception as e:
    print(e)
finally:
    print("File closed.")

