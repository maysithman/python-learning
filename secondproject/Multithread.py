from threading import Thread
from time import sleep


#use "Multithreading"
#I want to print the Main Thread ("Bye") after the end of printing "Hello" thread and "Hi" thread.


#this is "First Thread"
class Hello(Thread):
    def run(self):
        for i in range(10):
            print(i, "Hello")
            sleep(1)


#this is "Second Thread"
class Hi(Thread):
    def run(self):
        for i in range(10):
            print(i, "Hi")
            sleep(1)


thread_1 = Hello()
thread_2 = Hi()


thread_1.start()
sleep(0.2)
thread_2.start()


#print "Main Thread" after Thread 1 and 2.
thread_1.join()
thread_2.join()


#this is for "Main Thread"
print('Bye Bye.')

