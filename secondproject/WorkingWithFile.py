#Reading existing File and create new file by copying existing File, append more words at new file


old_file = open('MyProfileData', 'r')
new_file = open('Copy_MyProfileData', 'a')


for data in old_file:
    new_file.write(data)
new_file.write('\n \n Done.')


#Reading binary data of picture and copying it.
old_photo = open('profile_photo.jpg', 'rb')
new_photo = open('copy_profile_photo.jpg', 'wb')


for data in old_photo:
    new_photo.write(data)

