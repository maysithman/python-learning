class Team:
    def show_Team(self):
        print("This is our Team :")


class Testing(Team):
    TestingName= ""


class Dev(Team):
    DevName = ""

    def show_Dev(self):
        print(self.DevName)


class Sprint(Testing, Dev):
    def show_parent(self):
        print("Testing :", self.TestingName)
        print("Dev :", self.DevName)


s1 = Sprint()
s1.TestingName = "May Sit Hman"
s1.DevName = "Jin Jin"
s1.show_Team()
s1.show_parent()

