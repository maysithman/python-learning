#sort the data in the list by using BubbleSort


def bubble_sort(data):
    for i in range(len(data)-1, 0 , -1):
        for j in range(i):
            if data[j] > data[j+1]:
                temp = data[j]
                data[j] = data[j+1]
                data[j+1] = temp


data = [10, 2, 5, 7, 9, 2, 1, 4]


print('Before Sorting : ' + str(data))
bubble_sort(data)
print('After Sorting : ' + str(data))

