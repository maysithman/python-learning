#find number at the existing list by "Binary Search".


pos = -1


def search(data, find_num):
    lower_bond = 0
    upper_bond = len(data)-1

    if find_num > data[upper_bond]:
        return False
    else:
        while lower_bond <= upper_bond:
            mid_value = (lower_bond + upper_bond) // 2
            if data[mid_value] == find_num:
                globals()['pos'] = mid_value
                return True
            else:
                if data[mid_value] < find_num:
                    lower_bond = mid_value + 1
                else:
                    upper_bond = mid_value - 1
        return False


#the "data" list needs the sorted list for Binary Search
data = [2, 4, 5, 9, 11, 23, 32, 43, 44]
find_num = int(input('Insert Number that you want to find at the list : '))


if search(data, find_num):
    print('Found at the ' + str(pos) + ' index of list.')
else:
    print('Not Found.')

