from django.conf.urls import url
from django.contrib import admin
from blog_posts import views


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.post_list, name='post_list'),
    url(r'^new$', views.post_create, name='post_new'),
    url(r'^edit/(?P&lt;pk&gt;\d+)$', views.post_update, name='post_edit'),
    url(r'^delete/(?p&lt;pk&gt;\d+)$', views.post_delete, name='post_delete'),
]