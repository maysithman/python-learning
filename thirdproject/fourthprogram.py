import json
import os


# learn JSON
def learn_json():
    staff_dict = {'name':'may sit hmann', 'age':'24', 'sign':'cancer'}
    print(staff_dict)
    language_list = ['python', 'java', 'php']
    staff_dict['languages'] = language_list
    staff_json = json.dumps(staff_dict)
    print(staff_json)


learn_json()

# learn Managing keys
def learn_managingkeys():
    os_version = os.getenv('OS')
    print(os_version)
    password = os.getenv('PASSWORD')
    print(password)


learn_managingkeys()


# learn decorators
def logger(func):
    def wrapper():
        print('Logging executing.')
        func()
        print('Done Logging.')
    return wrapper


@logger
def sample():
    print('-----Inside sample function.')


sample()

