from datetime import datetime, timedelta


# learn date
def learn_date():
    today = datetime.now()
    print('Today is : ' + str(today))

    one_day = timedelta(days=1)
    yesterday = today - one_day
    print('Yesterday was : ' + str(yesterday))

    one_week = timedelta(weeks=1)
    last_week = today - one_week
    print('Last week was : ' + str(last_week))

    print('Day : ' + str(today.day))
    print('Month : ' + str(today.month))
    print('Year : ' + str(today.year))

    print('Hour : ' + str(today.hour))
    print('Minut : ' + str(today.minute))
    print('Second : ' + str(today.second))

    birthday = input("What's your birthday (dd/mm/yyyy)?")
    birthday_date = datetime.strptime(birthday, '%d/%m/%Y')
    print('My Birthday is ' + str(birthday_date))

    birthday_eve = birthday_date - one_day
    print('Date before birthday : ' + str(birthday_eve))

    birthday_weekeve = birthday_date - one_week
    print('Date before one week on birthday : ' + str(birthday_weekeve))

learn_date()