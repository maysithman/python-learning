from array import array
import datetime
from colorama import Fore, Back, Style
from colorama import init


# complex condition checking
def gpa_result():
    gpa = input('insert gpa grade : ')
    lowest_grade = input('insert lowest grade : ')

    gpa = float(gpa)
    lowest_grade = float(lowest_grade)

    if gpa >= .80 and lowest_grade >= .90:
        honour_roll = True
    else:
        honour_roll = False

    if honour_roll:
        print('Well done.')
    else:
        print('Fail.')


gpa_result()


#learn collections : list, array, dictionary
def learn_collections():
    names = ['may sit hman', 'jin jin', 'sandi', 'shwe shwe']
    print(names)

    scores = array('d')
    print(scores)

    dic = {'name':'may'}
    print(dic['name'])

    person = {}
    person['first'] = 'may'
    person['second'] = 'sit hman'
    print(person['first'])

    people = [dic, person]
    print(people[0])


learn_collections()


#learch conditions : for, while
def learn_conditions():
    for name in ['may', 'sit', 'hman']:
        print(name)

    for index in range(7,9):
        print(index)

    number = [1, 2, 3, 4, 5]
    index = 0
    while index < len(number):
        index = index+1

    for num in number:
        print(num)


learn_conditions()


#some practice functions
def print_time():
    time = datetime.datetime.now()
    return time


start_time = print_time()
print(start_time)


for index in range(0,100):
    print(index)


end_time = print_time()
print(end_time)


print('Different time : ' + str(end_time - start_time))


#some practice functions
def get_initial(name):
    initial_name = name[0:1].upper()
    return initial_name


first_name = input('Enter your first name : ')
second_name = input('Enter your second name : ')


print('The Initial Characters of Name are ' + get_initial(first_name) + ' and ' + get_initial(second_name))


#practice with color package
def learn_color_package():
    print(Fore.RED + 'some red text')
    print(Back.GREEN + 'and with a green background')
    print(Style.DIM + 'and in dim text')
    print(Style.RESET_ALL)
    print('back to normal now')

    init(autoreset=True)
    print(Fore.RED + 'some red text')
    print('automatically back to default color again')

